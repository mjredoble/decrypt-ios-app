# Decrypt iOS App using Frida and iProxy

This is a tutorial on how to decrypt an iOS Application using jailbroken iOS 13

## Tools Needed

- [ ] Install frida-ios-decrypt (https://github.com/AloneMonkey/frida-ios-dump)
- [ ] Install required pip packages from frida
- [ ] Install frida in your device 
<br> Start Cydia and add Frida’s repository by going to Manage -> Sources -> Edit -> Add and enter https://build.frida.re. You should now be able to find and install the Frida package which lets Frida inject JavaScript into apps running on your iOS device. This happens over USB, so you will need to have your USB cable handy, though there’s no need to plug it in just yet.<br> 
- [ ] Install iproxy
```
brew install usbmuxd
```


<!-- ## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:588174a1066b918f4bbaf9079bb51135?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:588174a1066b918f4bbaf9079bb51135?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:588174a1066b918f4bbaf9079bb51135?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:588174a1066b918f4bbaf9079bb51135?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:588174a1066b918f4bbaf9079bb51135?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html) -->

